import Exceptions.UserAlreadyExists;
import Exceptions.UserDoesNotExists;
import Models.User;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class GetBycicle_returnBycicle {
    /**
     *  Inserindo um IdUser que nao exista é lançada um exceção
     */
    @Test
    public void GetBycicle_idUser() {


        assertThrows(UserDoesNotExists.class,
                () -> {
                    BikeRentalSystem testee = new BikeRentalSystem(10);
                    testee.registerUser(1,"pedro",10);
                    List<User> users = testee.getUsers();
                    testee.getBicycle(10,2,10);
                });


    }
    /**
     *  Inserindo um IdUser que nao exista é lançada um exceção
     */

    @Test
    public void GetBycicle_idUserparaum() {


        assertThrows(UserDoesNotExists.class,
        () -> {
            BikeRentalSystem testee = new BikeRentalSystem(10);
            testee.registerUser(1,"pedro",10);
            List<User> users = testee.getUsers();
            testee.getBicycle(10,2,10);
        });


    }
    /**
     *  Inserindo um IdUser que nao exista é lançada um exceção
     */

    @Test
    public void GetBycicle_idUserparazero() {


        assertThrows(UserDoesNotExists.class,
                () -> {
                    BikeRentalSystem testee = new BikeRentalSystem(10);
                    testee.registerUser(0,"pedro",10);
                    List<User> users = testee.getUsers();
                    testee.getBicycle(10,2,10);
                });


    }
    /**
     *  Inserindo um IdUser que nao exista é lançada um exceção
     */

    @Test
    public void GetBycicle_idUserparamenosum() {


        assertThrows(UserDoesNotExists.class,
                () -> {
                    BikeRentalSystem testee = new BikeRentalSystem(10);
                    testee.registerUser(-1,"pedro",10);
                    List<User> users = testee.getUsers();
                    testee.getBicycle(10,2,10);
                });


    }
    /**Verificar a nao existencia de um deposito*/

    @Test
    public void GetBycicle_deposito() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem sytem = new BikeRentalSystem(10);
        sytem.registerUser(10,"pedro",1);
        sytem.addBicycle(1,20,15);
        assertEquals(-1 , sytem.getBicycle(11,10,1000));
    }


    @Test
    public void GetBycicle_depositoparaum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem sytem = new BikeRentalSystem(10);
        sytem.registerUser(10,"pedro",1);
        sytem.addBicycle(1,20,15);
        assertEquals(-1 , sytem.getBicycle(11,10,1000));
    }
    /**Verificar a nao existencia de um deposito*/

    @Test
    public void GetBycicle_depositoparazero() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem sytem = new BikeRentalSystem(10);
        sytem.registerUser(10,"pedro",1);
        sytem.addBicycle(0,20,15);
        assertEquals(-1 , sytem.getBicycle(11,10,1000));
    }
    /**Verificar a nao existencia de um deposito*/

    @Test
    public void GetBycicle_depositoparamenosum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem sytem = new BikeRentalSystem(10);
        sytem.registerUser(10,"pedro",1);
        sytem.addBicycle(-1,20,15);
        assertEquals(-1 , sytem.getBicycle(11,10,1000));
    }

   /** Verificar a existencia de credito por parte de um utilizador*/

    @Test

    public void GetBycicle_credito() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(10);
        testee.registerUser(10, "pedro", 1);
        List<User> users = testee.getUsers();
        testee.addCredit(users.get(0).getIDUser(), 0);
        assertEquals(-1, users.get(0).getCredit());

    }

    /** Verificar se nao existem bicicletas disponiveis */

@Test
    public void GetBycicle_bikes() throws UserDoesNotExists, UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(10);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(10,10,10);
        testee.getBicycle(10,10,10);
        assertEquals(-1,testee.getBicycle(10,10,1));


    }
    /** É  feito o "aluguer" por parte do utilizador e retornado o valor (id) da bicicleta */

    @Test
    public void GetBycicle_tudo() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro", 1);
        testee.addBicycle(1,1,1);
        testee.addCredit(10,1000);
        assertEquals(1,testee.getBicycle(1,10,1));


    }

    /** É  feito o "aluguer" por parte do utilizador e retornado o valor (id) da bicicleta */

    @Test
    public void GetBycicle_tudoparaum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro", 1);
        testee.addBicycle(1,1,1);
        testee.addCredit(10,1000);
        assertEquals(1,testee.getBicycle(1,10,1));


    }
    /** É  feito o "aluguer" por parte do utilizador e retornado o valor (id) da bicicleta */

    @Test
    public void GetBycicle_tudoparazero() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro", 1);
        testee.addBicycle(1,1,1);
        testee.addCredit(10,1000);
        assertEquals(1,testee.getBicycle(1,10,0));


    }
    /** É  feito o "aluguer" por parte do utilizador e retornado o valor (id) da bicicleta */

    @Test
    public void GetBycicle_tudoparamenosum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro", 1);
        testee.addBicycle(1,1,1);
        testee.addCredit(10,1000);
        assertEquals(1,testee.getBicycle(1,10,-1));


    }
    /** verificar iduser nao existente */

    @Test
    public  void returnBycicle_id() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(1,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(1,9,1000));

    }

    /** verificar iduser nao existente */

    @Test
    public  void returnBycicle_idparaum() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(1,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(1,9,1000));

    }
    /** verificar iduser nao existente */

    @Test
    public  void returnBycicle_idparazero() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(0,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(1,9,1000));

    }

    /** verificar iduser nao existente */

    @Test
    public  void returnBycicle_idparamenosum() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(-1,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(1,9,1000));

    }
    /** verificar iddeposito nao existente */

    @Test
    public  void returnBycicle_iddeposito() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(10,10,1000));

    }
        /** verificar iddeposito nao existente */

    @Test
    public  void returnBycicle_iddepositoparaum() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(1,1,1);

        assertEquals(-1,testee.returnBicycle(10,10,1000));

    }
    /** verificar iddeposito nao existente */

    @Test
    public  void returnBycicle_iddepositoparazero() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(0,1,1);

        assertEquals(-1,testee.returnBicycle(10,10,1000));

    }
    /** verificar iddeposito nao existente */

    @Test
    public  void returnBycicle_iddepositoparamenosum() throws UserAlreadyExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(-1,1,1);

        assertEquals(-1,testee.returnBicycle(10,10,1000));

    }
    /**
     * Passou mas nao devia porque com uma bike ativa nao deveria de retornar -1

     * @throws UserAlreadyExists
     * @throws UserDoesNotExists
     */

    @Test
    public  void returnBycicle_aluguer() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.addBicycle(1,1,1);
        testee.addLock(1,1);
        testee.getBicycle(1,10,1);


        assertEquals(-1,testee.returnBicycle(1,10,1000));


    }

  /**  Verificar a nao existencia de lugares livres*/

    @Test
    public  void returnBycicle_lugaresentradalivres() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.registerUser(11,"manel",1);
        testee.getBicycle(1,10,1000);
        testee.getBicycle(1,11,1000);

        testee.returnBicycle(1,10,1000);


        assertEquals(-1,testee.returnBicycle(1,11,1000));

    }

  /** Valor a pagar e resultado do saldo do cliente */

    @Test
    public  void returnBycicle_calculadavalorpagar() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.getBicycle(1,10,100);
        int amount = 1000;
        testee.addCredit(10,amount);
        int pagamento = testee.bicycleRentalFee(1,10,100,1);
        assertEquals(90 , pagamento);
        testee.addCredit(10, amount - pagamento);
        assertEquals(true,testee.verifyCredit(10));

    }
    @Test
    public  void returnBycicle_calculadavalorpagarparaoum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.getBicycle(1,10,100);
        int amount = 1000;
        testee.addCredit(10,amount);
        int pagamento = testee.bicycleRentalFee(1,10,100,1);
        assertEquals(90 , pagamento);
        testee.addCredit(10, amount - pagamento);
        assertEquals(true,testee.verifyCredit(10));

    }
    @Test
    public  void returnBycicle_calculadavalorpagarparazero() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.getBicycle(1,10,100);
        int amount = 1000;
        testee.addCredit(10,amount);
        int pagamento = testee.bicycleRentalFee(1,10,0,1);
        assertEquals(-10 , pagamento);
        testee.addCredit(10, amount - pagamento);
        assertEquals(true,testee.verifyCredit(10));

    }
    @Test
    public  void returnBycicle_calculadavalorpagarparamenosum() throws UserAlreadyExists, UserDoesNotExists {
        BikeRentalSystem testee = new BikeRentalSystem(1);
        testee.registerUser(10,"pedro",1);
        testee.getBicycle(1,10,100);
        int amount = 1000;
        testee.addCredit(10,amount);
        int pagamento = testee.bicycleRentalFee(1,10,-1,1);
        assertEquals(-11 , pagamento);
        testee.addCredit(10, amount - pagamento);
        assertEquals(true,testee.verifyCredit(10));

    }

}

