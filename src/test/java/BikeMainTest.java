import Exceptions.UserAlreadyExists;
import Models.User;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BikeMainTest {


    ///////////// Testes registerUser

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0 e String null e rentalProgram 1
     */
    @Test
    public void registerUserTesteID0NameNullRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,null, 1);
                    system.registerUser(0,null, 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0 e String preenchida e rentalProgram 1
     */
    @Test
    public void registerUserTesteID0NameStringRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,"Name", 1);
                    system.registerUser(0,"Name", 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1 e String null e rentalProgram 1
     */
    @Test
    public void registerUserTesteID1NameNullRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,null, 1);
                    system.registerUser(1,null, 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1 e String prennchida e rentalProgram 1
     */
    @Test
    public void registerUserTesteID1NameStringRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,"Name", 1);
                    system.registerUser(1,"Name", 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1 e String null e rentalProgram 1
     */
    @Test
    public void registerUserTesteIDNegativoNameNullRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,null, 1);
                    system.registerUser(-1,null, 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1 e String preenchida e rentalProgram 1
     */
    @Test
    public void registerUserTesteIDNegativoNameStringRental(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,"Name", 1);
                    system.registerUser(-1,"Name", 1);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0 e String preenchida e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteID0NameStringRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,"Name", 2);
                    system.registerUser(0,"Name", 2);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0, String null e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteID0NameNullRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,null, 2);
                    system.registerUser(0,null, 2);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1, String preenchida e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteID1NameStringRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,"Name", 2);
                    system.registerUser(1,"Name", 2);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1, String null e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteID1NameNullRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,null, 2);
                    system.registerUser(1,null, 2);
                });
    }
    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String preenchida e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteIDNegativoNameStringRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,"Name", 2);
                    system.registerUser(-1,"Name", 2);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String null e String preenchida e rentalProgram 2
     */
    @Test
    public void registerUserTesteIDNegativoNameNullRenta2(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,null, 2);
                    system.registerUser(-1,null, 2);
                });
    }



    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0, String null e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteID0NameNullRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,null, 3);
                    system.registerUser(0,null, 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0, String preenchida e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteID0NameStringRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,"Name", 3);
                    system.registerUser(0,"Name", 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1, String null e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteID1NameNullRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,null, 3);
                    system.registerUser(1,null, 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1, String preenchida e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteID1NameStringRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,"Name", 3);
                    system.registerUser(1,"Name", 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String null e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteIDNegativoNameNullRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,null, 3);
                    system.registerUser(-1,null, 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String preenchida e String preenchida e rentalProgram 3
     */
    @Test
    public void registerUserTesteIDNegativoNameStringRenta3(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,"Name", 3);
                    system.registerUser(-1,"Name", 3);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0, String null e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteID0NullStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,null, 0);
                    system.registerUser(0,null, 0);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 0, String preenchida e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteID0StringStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(0,"Name", 0);
                    system.registerUser(0,"Name", 0);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String null e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteID1NullStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,null, 0);
                    system.registerUser(1,null, 0);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser 1, String preenchida e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteID1StringStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(1,"Name", 0);
                    system.registerUser(1,"Name", 0);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String null e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteIDNegativoNullStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,null, 0);
                    system.registerUser(-1,null, 0);
                });
    }

    /**
     * Teste para para a possivel entrada de dois users iguais com valor IDUser -1, String preenchida e String preenchida e rentalProgram 0
     */
    @Test
    public void registerUserTesteIDNegativoStringStringRenta0(){
        assertThrows(UserAlreadyExists.class,
                ()->{
                    BikeRentalSystem system = new BikeRentalSystem(5);
                    system.registerUser(-1,"Name", 0);
                    system.registerUser(-1,"Name", 0);
                });
    }



    ////////////////////////// Testes addCredit


    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser0eAmountMaiorQue0() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 1;
        system.registerUser(0,"name",1);
        system.addCredit(0,0);
        List<User> users= system.getUsers();
        system.addCredit(0, amount);
        assertEquals(1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUserNegativoeAmountNegativo() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = -1;
        system.registerUser(-1,"name",1);
        system.addCredit(-1,0);
        List<User> users= system.getUsers();
        system.addCredit(-1, amount);
        assertEquals(-1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUserNegativoeAmount0() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 0;
        system.registerUser(-1,"name",1);
        system.addCredit(-1,0);
        List<User> users= system.getUsers();
        system.addCredit(-1, amount);
        assertEquals(0, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
        public void addCreditTesteUserNegativoeAmount1() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 1;
        system.registerUser(-1,"name",1);
        system.addCredit(-1,0);
        List<User> users= system.getUsers();
        system.addCredit(-1, amount);
        assertEquals(1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser0eAmount0() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 0;
        system.registerUser(0,"name",1);
        system.addCredit(0,1);
        List<User> users= system.getUsers();
        system.addCredit(0, amount);
        assertEquals(1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser0eAmountNegativo() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = -1;
        system.registerUser(0,"name",1);
        system.addCredit(0,0);
        List<User> users= system.getUsers();
        system.addCredit(0, amount);
        assertEquals(-1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser1eAmountNegativo() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = -1;
        system.registerUser(1,"name",1);
        system.addCredit(1,0);
        List<User> users= system.getUsers();
        system.addCredit(1, amount);
        assertEquals(-1, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser1eAmount0() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 0;
        system.registerUser(1,"name",1);
        system.addCredit(1,0);
        List<User> users= system.getUsers();
        system.addCredit(1, amount);
        assertEquals(0, users.get(0).getCredit());

    }

    /**
     * Teste para adicionar um credito a um utilizador e testar se o user não é null,
     * se o credito nao é null e se o credito esperado é realmente igual ao adicionado
     */
    @Test
    public void addCreditTesteUser1eAmount1() throws UserAlreadyExists {
        BikeRentalSystem system = new BikeRentalSystem(10);
        int amount = 1;
        system.registerUser(1,"name",1);
        system.addCredit(1,0);
        List<User> users= system.getUsers();
        system.addCredit(1, amount);
        assertEquals(1, users.get(0).getCredit());

    }


}
