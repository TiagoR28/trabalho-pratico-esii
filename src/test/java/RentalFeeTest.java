

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class RentalFeeTest {

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 1, 1, 2, 1
     */
    @Test
    void input_1_1_2_1() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals((2 - 1) * 1, bikeRentalSystem.bicycleRentalFee(1, 1, 2, 1));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 0, 0, 0, 0
     */
    @Test
    void input_0() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(0);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(0, 0, 0, 0));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 1, 2, 10
     */
    @Test
    void input_2_1_2_10() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(2, 1, 2, 10));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 1, 2, 3
     */
    @Test
    void input_2_1_2_3() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals((2 - 1) * 1, bikeRentalSystem.bicycleRentalFee(2, 1, 2, 3));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 5, 20, 3
     */
    @Test
    void input_2_5_20_3() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(10 * 1 + ((20 - 5) - 10) * 1 / 2, bikeRentalSystem.bicycleRentalFee(2, 5, 20, 3));
    }

    //Testes de fronteira, testando condições de entrada

    /**
     * Vai ser testado o metodo bikeRentalFee com o input -1, 5, 20, 3
     */
    @Test
    void input_rental_negative() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(-1, 5, 20, 3));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, -5, 20, 3
     */
    @Test
    void input_start_negative() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(2, -5, 20, 3));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 5, -20, 3
     */
    @Test
    void input_end_negative() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(2, 5, -20, 3));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 20, 5, 3
     */
    @Test
    void input_start_beats_end() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(2, 20, 5, 3));
    }

    /**
     * Vai ser testado o metodo bikeRentalFee com o input 2, 5, 20, -3
     */
    @Test
    void input_nRentals_negative() {
        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);

        assertEquals(0, bikeRentalSystem.bicycleRentalFee(2, 5, 20, -3));
    }
}
