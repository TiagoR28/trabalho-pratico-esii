import Exceptions.UserAlreadyExists;
import org.junit.jupiter.api.Test;
import Models.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VerefyCreditTest {

    /**
     * Vai testar o metodo verifyCredit e vai ser criado um utilizador  valido
     * @throws UserAlreadyExists
     */
    @Test
    public  void UserExist() throws UserAlreadyExists {

        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);
        bikeRentalSystem.registerUser(1, "Hugo", 1);

        assertEquals(true, bikeRentalSystem.verifyCredit(1));
    }

    /**
     * Vai testar o metodo verifyCredit e vai ser usado um utilizador que não existe
     * @throws UserAlreadyExists
     */
    @Test
    public  void UserNotExist() throws UserAlreadyExists {

        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);
        bikeRentalSystem.registerUser(1, "Hugo", 1);

        assertEquals(false, bikeRentalSystem.verifyCredit(2));
    }

    /**
     * Vai testar o metodo verifyCredit e vai ser usado um utilizador que o id negativo
     * @throws UserAlreadyExists
     */
    @Test
    public  void UserIdNegativeNotExist() throws UserAlreadyExists {

        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);
        bikeRentalSystem.registerUser(1, "Hugo", 1);

        assertEquals(false, bikeRentalSystem.verifyCredit(-2));
    }

    /**
     * Vai testar o metodo verifyCredit e vai ser usado um utilizador que o id negativo
     * @throws UserAlreadyExists
     */
    @Test
    public  void UserIdNegativeExist() throws UserAlreadyExists {

        BikeRentalSystem bikeRentalSystem = new BikeRentalSystem(1);
        bikeRentalSystem.registerUser(1, "Hugo", 1);

        assertEquals(false, bikeRentalSystem.verifyCredit(-1));
    }
}
